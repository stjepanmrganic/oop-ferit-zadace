#pragma once

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>

using std::istream;
using std::ostream;
using std::ifstream;
using std::ofstream;
using std::string;
using std::cout;
using std::cin;
using std::endl;

class Description
{
public:
	Description();
	Description(int, int, string);
	~Description();
	friend ostream& operator<<(ostream&, const Description&);
	friend istream& operator>>(istream&, Description&);
	Description& operator=(const Description&);
	int get_ep_num() const;
	int get_duration() const;
	string get_name() const;
	void set_ep_num(int);
	void set_duration(int);
	void set_name(string);
private:
	int mep_num, mduration;
	string mname;
};

class Episode
{
public:
	Episode(int, float, float);
	Episode(int, float, float, Description&);
	Episode();
	~Episode();
	friend ostream& operator<<(ostream&, const Episode&);
	friend istream& operator>>(istream&, Episode&);
	Episode& operator=(const Episode&);
	void addView(float);
	float getMaxScore() const;
	float getAverageScore() const;
	int getViewerCount() const;
	Description get_mdes() const;
private:
	int mviewers;
	float msum, mrating;
	Description mdes;
};

float generateRandomScore();

void print(Episode**, int);

void sort(Episode**, int);

void persistToFile(string, Episode**, int);