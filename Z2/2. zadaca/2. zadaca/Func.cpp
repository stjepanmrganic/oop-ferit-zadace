#include "Header.h"

Episode::Episode(int viewers, float sum, float rating) : mviewers(viewers), msum(sum), mrating(rating) { }

Episode::Episode(int viewers, float sum, float rating, Description& des) :
mviewers(viewers), msum(sum), mrating(rating), mdes(des) { }

Episode::Episode() : mviewers(0), msum(0), mrating(0), mdes(){ }

Episode::~Episode() { }

ostream& operator<<(ostream& out, const Episode& ep) {
	out << ep.mviewers << ", " << ep.msum << ',' << ep.mrating << ',' << ep.mdes;
	return out;
}

istream& operator>>(istream& in, Episode& ep) {
	char bin;
	in >> ep.mviewers >> bin >> ep.msum >> bin >> ep.mrating >> bin >> ep.mdes;
	return in;
}

Episode& Episode::operator=(const Episode& ref) {
	mviewers = ref.mviewers;
	msum = ref.msum;
	mrating = ref.mrating;
	mdes = ref.mdes;
	return *this;
}

void Episode::addView(float num) { mviewers++; msum += num; if (mrating < num) mrating = num; }

float Episode::getMaxScore() const { return mrating; }

float Episode::getAverageScore() const { return msum / mviewers; }

int Episode::getViewerCount() const { return mviewers; }

Description Episode::get_mdes() const { return mdes; }

Description::Description() { }

Description::Description(int ep_num, int duration, string name) : mep_num(ep_num), mduration(duration), mname(name) { }

Description::~Description() { }

ostream& operator<<(ostream& out, const Description& des) {
	out << des.mep_num << ',' << des.mduration << ',' << des.mname;
	return out;
}

istream& operator>>(istream& in, Description& des) {
	char bin;
	in >> des.mep_num >> bin >> des.mduration >> bin;
	getline(in, des.mname);
	return in;
}

Description& Description::operator=(const Description& ref) {
	mep_num = ref.mep_num;
	mduration = ref.mduration;
	mname = ref.mname;
	return *this;
}

int Description::get_ep_num() const { return mep_num; }

int Description::get_duration() const { return mduration; }

string Description::get_name() const { return mname; }

void Description::set_ep_num(int ep_num) { mep_num = ep_num; }

void Description::set_duration(int dur) { mduration = dur; }

void Description::set_name(string name) { mname = name; }

float generateRandomScore() { return ((float)rand() / (float)RAND_MAX) * 10; }

void print(Episode** ep, int n) {
	for (int i = 0; i < n; i++) {
		cout << *ep[i] << endl;
	}
}

void sort(Episode** ep, int n) {
	int i, j, stop;
	Episode swap;
	for (i = 0, stop = 1; stop; i++) {
		stop = 0;
		for (j = 0; j < n - i - 1; j++) {
			string help = ep[j]->get_mdes().get_name();
			string help2 = ep[j + 1]->get_mdes().get_name();
			if (help2[0] < help[0]) {
				swap = *ep[j + 1];
				*ep[j + 1] = *ep[j];
				*ep[j] = swap;
				stop = 1;
			}
		}
	}
}

void persistToFile(string name, Episode** ep, int n) {
	ofstream out(name);
	for (int i = 0; i < n; i++) {
		out << *ep[i] << endl;
	}
	out.close();
}