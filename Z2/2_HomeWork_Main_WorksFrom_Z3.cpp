#include "Global_Functions.h"


int main() {
	Description description(1, 45, "Pilot");
	cout << description << endl;
	Episode episode(10, 88.64, 9.78, description);
	cout << episode << endl;

	// Assume that the number of rows in the text file is always at least 10. 
	// Assume a valid input file.
	string fileName("shows.tv");
	const int COUNT = 10;

	ifstream input(fileName);
	Episode* episodes[COUNT];

	if (input.is_open() == false)
		return 1;

	for (int i = 0; i < COUNT; i++) {
		episodes[i] = new Episode();
		input >> *episodes[i];
	}
	input.close();

	cout << "Episodes:" << endl;
	Print(episodes, COUNT);
	Sort(episodes, COUNT);
	cout << "Sorted episodes:" << endl;
	Print(episodes, COUNT);

	PersistToFile("sorted.tv", episodes, COUNT);

	for (int i = 0; i < COUNT; i++) {
		delete episodes[i];
	}

	return 0;
}