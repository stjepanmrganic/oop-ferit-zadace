#pragma once
#include "Season.h"

class IPrinter
{
public:
	virtual ~IPrinter();
	virtual void print(const Season& Reference) = 0;
	virtual void print(const EpisodeNotFoundException& ex) = 0;
};