#pragma once
#include "IPrinter.h"
#include <string>
#include <fstream>


class FilePrinter : public IPrinter
{
private:
	std::string mFileName;
	std::ofstream mLog;
public:
	FilePrinter(std::string FileName);
	virtual ~FilePrinter();
	virtual void print(const Season& Reference);
	virtual void print(const EpisodeNotFoundException& ex);
};

