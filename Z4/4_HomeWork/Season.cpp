#include "Season.h"


Season::Season() { }

Season::Season(const Season& Reference) {
	for (unsigned int i = 0; i < Reference.mVectorEp.size(); i++) {
		mVectorEp.push_back(Reference.mVectorEp[i]);
	}
}

Season::Season(const vector<Episode>& Episodes) {
	for (unsigned int i = 0; i < Episodes.size(); i++) {
		mVectorEp.push_back(Episodes[i]);
	}
}

Season::~Season() { }

double Season::GetAverageRating() const {
	double RatingSum = 0;
	for (unsigned int i = 0; i < mVectorEp.size(); i++) {
		RatingSum += mVectorEp[i].GetAverageScore();
	}
	return RatingSum / mVectorEp.size();
}

int Season::GetTotalViews() const {
	int TotalViews = 0;
	for (unsigned int i = 0; i < mVectorEp.size(); i++) {
		TotalViews += mVectorEp[i].GetViewerCount();
	}
	return TotalViews;
}

const vector<Episode>& Season::GetVectorEp() const { return mVectorEp; }

const Episode& Season::GetBestEpisode() const {
	double MaxScore = 0;
	int BestEpisodeIndex = 0;
	for (unsigned int i = 0; i < mVectorEp.size(); i++) {
		if (MaxScore < mVectorEp[i].GetMaxScore()) {
			MaxScore = mVectorEp[i].GetMaxScore();
			BestEpisodeIndex = i;
		}
	}
	return mVectorEp[BestEpisodeIndex];
}

Season& Season::operator=(const Season& Reference) {
	if (this != &Reference) {
		for (unsigned int i = 0; i < mVectorEp.size(); i++) {
			mVectorEp.push_back(Reference.mVectorEp[i]);
		}
	}
	return *this;
}

const Episode& Season::operator[](int Index) const {
	return mVectorEp[Index];
}

void Season::add(const Episode& EpisodeToAppend) {
	mVectorEp.push_back(EpisodeToAppend);
}

void Season::remove(const string& EpisodeName) {
	unsigned int i = 0;
	for (i; i < mVectorEp.size(); i++) {
		if (mVectorEp[i].GetDescription().GetName() == EpisodeName) {
			for (unsigned int j = i; j < mVectorEp.size() - 1; j++) {
				mVectorEp[j] = mVectorEp[j + 1];
			}
			mVectorEp.resize(mVectorEp.size() - 1);
			break;
		}
	}
	if (i == mVectorEp.size()) {
		throw EpisodeNotFoundException(EpisodeName);
	}
}
