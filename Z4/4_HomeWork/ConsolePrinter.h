#pragma once
#include "IPrinter.h"
class ConsolePrinter : public IPrinter
{
private:

public:
	ConsolePrinter();
	virtual ~ConsolePrinter();
	virtual void print(const Season& SeasonForPrint);
	virtual void print(const EpisodeNotFoundException& ex);
};

