#pragma once
#include "Description.h"


class Episode
{
public:

	Episode(int Viewers, double Sum, double Rating);
	Episode(int Viewers, double Sum, double Rating, Description& Description);
	Episode(int Viewers, double Sum, double Rating, Description Description);
	Episode();
	~Episode();
	Episode& operator=(const Episode& Reference);
	
	void AddView(double Number);
	double GetMaxScore() const;
	double GetAverageScore() const;
	int GetViewerCount() const;
	Description GetDescription() const;

	friend ostream& operator<<(ostream& Out, const Episode& Episode);
	friend istream& operator>>(istream& In, Episode& Episode);
	friend bool operator==(const Episode& OperandOne, const Episode& OperandTwo);

private:
	int mViewers;
	double mSum, mRating;
	Description mDescription;
};