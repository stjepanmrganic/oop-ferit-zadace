#include "Global_Functions.h"


float GenerateRandomScore() { return ((float)rand() / (float)RAND_MAX) * 10; }

void Print(Episode** Episodes, int NumberOfEpisodes) {
	for (int i = 0; i < NumberOfEpisodes; i++) {
		cout << *Episodes[i] << endl;
	}
}

void Sort(Episode** Episodes, int NumberOfEpisodes) {
	int i, j, stop;
	Episode swap;
	for (i = 0, stop = 1; stop; i++) {
		stop = 0;
		for (j = 0; j < NumberOfEpisodes - i - 1; j++) {
			if(Episodes[j + 1]->GetDescription() < Episodes[j]->GetDescription()) {
				swap = *Episodes[j + 1];
				*Episodes[j + 1] = *Episodes[j];
				*Episodes[j] = swap;
				stop = 1;
			}
		}
	}
}

void PersistToFile(string Name, Episode** Episodes, int NumberOfEpisodes) {
	ofstream Out(Name);
	for (int i = 0; i < NumberOfEpisodes; i++) {
		Out << *Episodes[i] << endl;
	}
	Out.close();
}

int StrLen(string Operand) {
	int i = 0;
	for (i; Operand[i] != '\0'; i++);
	return i;
}

string LowerCaseConvert(string Operand) {
	for (int i = 0; Operand[i] != '\0'; i++) {
		Operand[i] = tolower(Operand[i]);
	}
	return Operand;
}

Episode** loadEpisodesFromFile(string fileName, const int COUNT) {

	fstream EpisodeFile(fileName, ios::in);
	if (EpisodeFile.is_open() == false) {
		cout << "No episode file." << endl;
		exit(-1);
	}
	else {
		Episode** Episodes = new Episode*[COUNT];
		for (int i = 0; i < COUNT; i++) {
			Episodes[i] = new Episode();
			EpisodeFile >> *Episodes[i];
		}
		EpisodeFile.close();
		return Episodes;
	}
}

std::vector<Episode> loadEpisodesFromFile(string fileName) {
	fstream EpisodeFile(fileName, ios::in);
	if (EpisodeFile.is_open() == false) {
		throw -1;
	}
	else {
		std::vector<Episode> Episodes;
		Episode Temp;
		while (EpisodeFile >> Temp) {
			Episodes.push_back(Temp);
		}
		EpisodeFile.close();
		return Episodes;
	}
}
