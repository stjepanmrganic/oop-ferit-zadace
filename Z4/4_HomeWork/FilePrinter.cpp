#include "FilePrinter.h"


FilePrinter::FilePrinter(std::string FileName) : mFileName(FileName) { mLog.open(FileName); }

FilePrinter::~FilePrinter() { mLog.close(); }

void FilePrinter::print(const Season& Reference) {
	mLog << "******************************" << endl;
	for (unsigned int i = 0; i < Reference.GetVectorEp().size(); i++) {
		mLog << Reference.GetVectorEp()[i] << endl;
	}
	mLog << "******************************" << endl << endl;
}

void FilePrinter::print(const EpisodeNotFoundException & ex) {
	mLog << ex.what() << ", Name: " << ex.getName() << "\n\n";
}
