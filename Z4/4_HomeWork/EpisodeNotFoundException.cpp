#include "EpisodeNotFoundException.h"


EpisodeNotFoundException::EpisodeNotFoundException(const std::string& EpisodeName) : std::runtime_error("No such episode found."), mEpisodeName(EpisodeName) { }

EpisodeNotFoundException::~EpisodeNotFoundException() { }

const std::string EpisodeNotFoundException::getName() const { return mEpisodeName; }
