#pragma once
#include <stdexcept>
#include <string>
class EpisodeNotFoundException : public std::runtime_error
{
private:
	std::string mEpisodeName;
public:
	EpisodeNotFoundException(const std::string& EpisodeName);
	~EpisodeNotFoundException();
	const std::string getName() const;
};

