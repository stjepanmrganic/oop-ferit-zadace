#pragma once
#include "Episode.h"
#include "EpisodeNotFoundException.h"


class Season
{
private:
	vector<Episode> mVectorEp;
public:
	Season();
	Season(const Season& Reference);
	Season(const vector<Episode>& Episodes);
	~Season();

	double GetAverageRating() const;
	int GetTotalViews() const;
	const vector<Episode>& GetVectorEp() const;
	const Episode& GetBestEpisode() const;

	Season& operator =(const Season& Reference);
	const Episode& operator[](int Index) const;

	void add(const Episode& EpisodeToAppend);
	void remove(const string& EpisodeName);
};