
#include "Global_Functions.h"
#include "ConsolePrinter.h"
#include "FilePrinter.h"

int main() {

	IPrinter* printer = new ConsolePrinter();
	//IPrinter* printer = new FilePrinter("name.txt");

	std::string fileName("shows.tv");
	std::vector<Episode> episodes = loadEpisodesFromFile(fileName);
	Season* season = new Season(episodes);

	printer->print(*season);
	season->add(Episode(10, 84.56, 9.88, Description(11, 45, "Christmas special")));
	printer->print(*season);
	try {
		season->remove("Pilot");
		season->remove("Nope.");
	}
	catch (const EpisodeNotFoundException& ex) {
		printer->print(ex);
	}
	printer->print(*season);
	delete printer;
	delete season;

	system("Pause");
	return 0;
}