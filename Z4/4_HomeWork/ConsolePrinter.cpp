#include "ConsolePrinter.h"


ConsolePrinter::ConsolePrinter() { }

ConsolePrinter::~ConsolePrinter() { }

void ConsolePrinter::print(const Season& SeasonForPrint) {
	cout << "******************************" << endl;
	for (unsigned int i = 0; i < SeasonForPrint.GetVectorEp().size(); i++) {
		cout << SeasonForPrint.GetVectorEp()[i] << endl;
	}
	cout << "******************************" << endl << endl;
}

void ConsolePrinter::print(const EpisodeNotFoundException & ex) {
	std::cout << ex.what() << ", Name: " << ex.getName() << "\n\n";
}
