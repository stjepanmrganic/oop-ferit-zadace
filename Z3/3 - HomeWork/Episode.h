#pragma once
#include "Description.h"


class Episode
{
public:

	Episode(int Viewers, float Sum, float Rating);
	Episode(int Viewers, float Sum, float Rating, Description& Description);
	Episode();
	~Episode();
	Episode& operator=(const Episode& Reference);
	
	void AddView(float Number);
	float GetMaxScore() const;
	float GetAverageScore() const;
	int GetViewerCount() const;
	Description GetDescription() const;

	friend ostream& operator<<(ostream& Out, const Episode& Episode);
	friend istream& operator>>(istream& In, Episode& Episode);
	friend bool operator==(const Episode& OperandOne, const Episode& OperandTwo);

private:
	int mViewers;
	float mSum, mRating;
	Description mDescription;
};