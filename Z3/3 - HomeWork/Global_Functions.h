#pragma once
#include "Episode.h"
#include <cctype>


float GenerateRandomScore();

void Print(Episode** Episodes, int NumberOfEpisodes);

void Sort(Episode** Episodes, int NumberOfEpisodes);

void PersistToFile(string Name, Episode** Episode, int NumberOfEpisodes);

int StrLen(string Operand);

string LowerCaseConvert(string Operand);

Episode** loadEpisodesFromFile(string fileName, const int COUNT);