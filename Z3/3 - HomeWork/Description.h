#pragma once
#include <iostream>
#include <fstream>
#include <string>
using namespace std;


class Description
{
public:

	Description();
	Description(int EpisodeNumber, int Duration, string Name);
	~Description();
	Description& operator=(const Description& Reference);

	int GetEpisodeNumber() const;
	int GetDuration() const;
	string GetName() const;
	void SetEpisodeNumber(int EpisodeNumber);
	void SetDuration(int Duration);
	void SetName(string Name);

	friend ostream& operator<<(ostream& Out, const Description& Description);
	friend istream& operator>>(istream& In, Description& Description);
	friend bool operator<(const Description& OperandOne, const Description& OperandTwo);
	friend bool operator==(const Description& OperandOne, const Description& OperandTwo);

private:
	int mEpisodeNumber, mDuration;
	string mName;
};

