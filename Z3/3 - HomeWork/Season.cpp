#include "Season.h"


Season::Season() { }

Season::Season(const Season& Reference) : mEpisodeAmount(Reference.mEpisodeAmount) {
	mEpisodes = new Episode*[mEpisodeAmount];
	for (int i = 0; i < mEpisodeAmount; i++) {
		mEpisodes[i] = new Episode();
		*mEpisodes[i] = *Reference.mEpisodes[i];
	}
}

Season::Season(Episode** Episodes, const int COUNT) : mEpisodeAmount(COUNT) {
	mEpisodes = new Episode*[mEpisodeAmount];
	for (int i = 0; i < mEpisodeAmount; i++) {
		mEpisodes[i] = new Episode();
		*mEpisodes[i] = *Episodes[i];
	}
}

Season::~Season() {
	for (int i = 0; i < this->mEpisodeAmount; i++) {
		delete this->mEpisodes[i];
	}
	delete[] this->mEpisodes;
}

float Season::GetAverageRating() const {
	float RatingSum = 0;
	for (int i = 0; i < mEpisodeAmount; i++) {
		RatingSum += mEpisodes[i]->GetAverageScore();
	}
	return RatingSum / mEpisodeAmount;
}

int Season::GetTotalViews() const {
	int TotalViews = 0;
	for (int i = 0; i < mEpisodeAmount; i++) {
		TotalViews += mEpisodes[i]->GetViewerCount();
	}
	return TotalViews;
}

const Episode& Season::GetBestEpisode() const {
	float MaxScore = 0;
	int BestEpisodeIndex = 0;
	for (int i = 0; i < mEpisodeAmount; i++) {
		if (MaxScore < mEpisodes[i]->GetMaxScore()) {
			MaxScore = mEpisodes[i]->GetMaxScore();
			BestEpisodeIndex = i;
		}
	}
	return *mEpisodes[BestEpisodeIndex];
}

Season& Season::operator=(const Season& Reference) {
	if (this != &Reference) {
		mEpisodeAmount = Reference.mEpisodeAmount;
		mEpisodes = new Episode*[mEpisodeAmount];
		for (int i = 0; i < mEpisodeAmount; i++) {
			mEpisodes[i] = new Episode();
			*mEpisodes[i] = *Reference.mEpisodes[i];
		}
	}
	return *this;
}

const Episode& Season::operator[](int Index) const {
	return *this->mEpisodes[Index];
}
