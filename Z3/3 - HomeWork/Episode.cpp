#include "Episode.h"


Episode::Episode(int Viewers, float Sum, float Rating)
	: mViewers(Viewers), mSum(Sum), mRating(Rating) { }

Episode::Episode(int Viewers, float Sum, float Rating, Description& Description) :
	mViewers(Viewers), mSum(Sum), mRating(Rating), mDescription(Description) { }

Episode::Episode() : mViewers(0), mSum(0), mRating(0), mDescription() { }

Episode::~Episode() { }

Episode& Episode::operator=(const Episode& Reference) {
	mViewers = Reference.mViewers;
	mSum = Reference.mSum;
	mRating = Reference.mRating;
	mDescription = Reference.mDescription;
	return *this;
}

void Episode::AddView(float Number) { mViewers++; mSum += Number; if (mRating < Number) mRating = Number; }

float Episode::GetMaxScore() const { return mRating; }

float Episode::GetAverageScore() const { return mSum / mViewers; }

int Episode::GetViewerCount() const { return mViewers; }

Description Episode::GetDescription() const { return mDescription; }

ostream& operator<<(ostream& Out, const Episode& Episode) {
	Out << Episode.mViewers << ", " << Episode.mSum << ',' << Episode.mRating << ',' << Episode.mDescription;
	return Out;
}

istream& operator>>(istream& In, Episode& Episode) {
	char bin;
	In >> Episode.mViewers >> bin >> Episode.mSum >> bin >> Episode.mRating >> bin >> Episode.mDescription;
	return In;
}

bool operator==(const Episode& OperandOne, const Episode& OperandTwo) {
	if (OperandOne.mDescription == OperandTwo.mDescription) {
		if (OperandOne.mRating == OperandTwo.mRating && OperandOne.mSum == OperandTwo.mSum && OperandOne.mViewers == OperandTwo.mViewers) {
			return true;
		}
		else {
			return false;
		}
	}
	else {
		return false;
	}
}