#pragma once
#include "Episode.h"


class Season
{
private:
	int mEpisodeAmount;
	Episode** mEpisodes;
public:
	Season();
	Season(const Season& Reference);
	Season(Episode** Episodes, const int COUNT);
	~Season();

	float GetAverageRating() const;
	int GetTotalViews() const;
	const Episode& GetBestEpisode() const;

	Season& operator =(const Season& Reference);
	const Episode& operator[](int Index) const;
};