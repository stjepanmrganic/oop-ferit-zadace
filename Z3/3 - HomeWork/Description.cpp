#include "Description.h"
#include "Global_Functions.h"


Description::Description() { }

Description::Description(int EpisodeNumber, int Duration, string Name) : mEpisodeNumber(EpisodeNumber), mDuration(Duration), mName(Name) { }

Description::~Description() { }

Description& Description::operator=(const Description& Reference) {
	mEpisodeNumber = Reference.mEpisodeNumber;
	mDuration = Reference.mDuration;
	mName = Reference.mName;
	return *this;
}

int Description::GetEpisodeNumber() const { return mEpisodeNumber; }

int Description::GetDuration() const { return mDuration; }

string Description::GetName() const { return mName; }

void Description::SetEpisodeNumber(int EpisodeNumber) { mEpisodeNumber = EpisodeNumber; }

void Description::SetDuration(int Duration) { mDuration = Duration; }

void Description::SetName(string Name) { mName = Name; }

ostream& operator<<(ostream& Out, const Description& Description) {
	Out << Description.mEpisodeNumber << ',' << Description.mDuration << ',' << Description.mName;
	return Out;
}

istream& operator>>(istream& In, Description& Description) {
	char bin;
	In >> Description.mEpisodeNumber >> bin >> Description.mDuration >> bin;
	getline(In, Description.mName);
	return In;
}

bool operator<(const Description& OperandOne, const Description& OperandTwo) {

	int OperandOneLen = StrLen(OperandOne.mName);
	int OperandTwoLen = StrLen(OperandTwo.mName);
	string OperandOneSmallCaps = LowerCaseConvert(OperandOne.mName);
	string OperandTwoSmallCaps = LowerCaseConvert(OperandTwo.mName);

	if (OperandOneLen < OperandTwoLen) {
		for (int i = 0; i < OperandOneLen; i++) {
			if (OperandOneSmallCaps[i] < OperandTwoSmallCaps[i]) {
				return true;
			}
			else if (OperandOneSmallCaps[i] == OperandTwoSmallCaps[i]) {
				continue;
			}
			else {
				return false;
			}
		}
	}
	else {
		for (int i = 0; i < OperandTwoLen; i++) {
			if (OperandOneSmallCaps[i] < OperandTwoSmallCaps[i]) {
				return true;
			}
			else if (OperandOneSmallCaps[i] == OperandTwoSmallCaps[i]) {
				continue;
			}
			else {
				return false;
			}
		}
	}
	return true;
}

bool operator==(const Description& OperandOne, const Description& OperandTwo) {
	if (OperandOne.mName == OperandTwo.mName && OperandOne.mEpisodeNumber == OperandTwo.mEpisodeNumber && OperandOne.mDuration == OperandTwo.mDuration) {
		return true;
	}
	else {
		return false;
	}
}