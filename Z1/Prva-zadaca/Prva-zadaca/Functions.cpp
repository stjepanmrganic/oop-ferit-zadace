#include "Header.h"
#include <stdlib.h>

Episode::Episode(int viewers, float sum, float rating) : mviewers(viewers), msum(sum), mrating(rating) { }

Episode::Episode() : mviewers(0), msum(0), mrating(0) { }

Episode::~Episode() { }

void Episode::addView(float num) { mviewers++; msum += num; if (mrating < num) mrating = num; }

float Episode::getMaxScore() { return mrating; }

float Episode::getAverageScore() { return msum / mviewers; }

int Episode::getViewerCount() { return mviewers; }

float generateRandomScore() { return ((float)rand() / (float)RAND_MAX) * 10; }