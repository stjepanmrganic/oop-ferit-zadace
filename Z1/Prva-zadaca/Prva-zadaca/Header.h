class Episode
{
public:
	Episode(int, float, float);
	Episode();
	~Episode();
	void addView(float);
	float getMaxScore();
	float getAverageScore();
	int getViewerCount();
private:
	int mviewers;
	float msum, mrating;
};

float generateRandomScore();