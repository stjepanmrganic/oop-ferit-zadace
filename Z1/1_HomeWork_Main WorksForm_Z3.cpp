#include "Episode.h"
#include "Global_Functions.h"


int main() {
	//srand((unsigned)time(NULL));
	/* Obrisati komentare za stvarne random vrijednosti i uljuciti time.h gore */
	Episode *ep1, *ep2;
	ep1 = new Episode();
	ep2 = new Episode(10, 64.39, 8.7);
	int viewers = 10;
	for (int i = 0; i < viewers; i++) {
		ep1->AddView(GenerateRandomScore());
		std::cout << ep1->GetMaxScore() << std::endl;
	}
	if (ep1->GetAverageScore() > ep2->GetAverageScore()) {
		std::cout << "Viewers: " << ep1->GetViewerCount() << std::endl;
	}
	else {
		std::cout << "Viewers: " << ep2->GetViewerCount() << std::endl;
	}
	delete ep1;
	delete ep2;
}